//////////////////////////////////////////////////////
// 程序名称：自动生成，待补充
// 功 能：自动生成，待补充
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-3-5
#include <conio.h>
#include <graphics.h>
#include <cmath>

void DDAline(int x1,int y1,int x2,int y2);

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//填充背景为白色
	setbkcolor(WHITE);
	cleardevice();
	//开始绘制
	setlinecolor(RED);
	
	int xl[] = {-80,80,-80,80};
	int yl[] = {-50,-50,50,50};

	for(int i=0;i<4;++i){
		DDAline(200,200,200+xl[i],200+yl[i]);
	}

	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
	return 0;
}

void DDAline(int x1,int y1,int x2,int y2){
	float dx=1.0f,dy=1.0f;
	COLORREF color = getlinecolor();
	if(y2==y1 && x2==x1);
	else if(abs(y2-y1) < abs(x2-x1)){
		if(x2<x1)dx=-1.0f;
		dy = float(y2-y1)/abs(x2-x1);
		for(float x=x1,y=y1;int(x+0.5)!=x2;x+=dx,y+=dy)
			putpixel(int(x+0.5),int(y+0.5),color);
	}
	else{
		if(y2<y1)dy=-1.0f;
		dx = float(x2-x1)/abs(y2-y1);
		for(float x=x1,y=y1;int(y+0.5)!=y2;x+=dx,y+=dy)
			putpixel(int(x+0.5),int(y+0.5),color);
	}
	putpixel(x2,y2,color);
	circle(x2,y2,10);
}