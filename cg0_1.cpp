//////////////////////////////////////////////////////
// 程序名称：一笔绘制图形
// 功 能：一笔绘制图中图形，带有动画演示效果
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-2-28
#include <conio.h>
#include <graphics.h>
#include <cmath>

//延时画线函数
void Lineto(int x, int y);

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//填充背景为白色
	setbkcolor(WHITE);
	cleardevice();
	//开始绘制
	moveto(160, 240);
	int dx = 0;
	setlinecolor(RGB(255, 0, 0));
	for (int i = 0; i < 9; ++i) {
		Lineto(160 + dx, 120);
		Lineto(480, 240);
		Lineto(160 + dx, 360);
		Lineto(160, 240);
		Lineto(480 - dx, 120);
		Lineto(480, 240);
		Lineto(480 - dx, 360);
		Lineto(160, 240);
		dx += 20;
	}
	Lineto(320, 120);
	Lineto(480, 240);
	Lineto(320, 360);
	Lineto(160, 240);
	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
}

void Lineto(int x,int y) {
	Sleep(100);
	lineto(x, y);
}
