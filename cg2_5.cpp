//////////////////////////////////////////////////////
// 程序名称：用其它算法实现多边形裁剪（选做）
// 功 能：裁剪后可以选择用填充多边形画图
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-3-21
#include <conio.h>
#include <graphics.h>
#include <cmath>
#include <list>
#include <utility>

COLORREF currentColor = RED;

void MPLline(int x1, int y1, int x2, int y2, COLORREF color)
{
	int a, b, d1, d2, d, x, y;
	if (x1 > x2)
	{
		x1 ^= x2;
		x2 ^= x1;
		x1 ^= x2;
		y1 ^= y2;
		y2 ^= y1;
		y1 ^= y2;
	}
	x = x2 - x1;
	y = y2 - y1;
	if (y >= x)
	{
		a = x1 - x2;
		b = y2 - y1;
		d = 2 * a + b;
		d1 = 2 * (a + b);
		d2 = 2 * a;
		x = x1;
		y = y1;
		putpixel(x, y, color);
		while (y < y2)
		{
			if (d < 0)
			{
				x++;
				y++;
				d += d1;
			}
			else
			{
				y++;
				d += d2;
			}
			putpixel(x, y, color);
		}
	}
	else if (y <= x && y >= 0)
	{
		a = y1 - y2;
		b = x2 - x1;
		d = 2 * a + b;
		d1 = 2 * (a + b);
		d2 = 2 * a;
		x = x1;
		y = y1;
		putpixel(x, y, color);
		while (x < x2)
		{
			if (d < 0)
			{
				x++;
				y++;
				d += d1;
			}
			else
			{
				x++;
				d += d2;
			}
			putpixel(x, y, color);
		}
	}
	else if (y >= -x)
	{
		a = y2 - y1;
		b = x2 - x1;
		d = 2 * a + b;
		d1 = 2 * (a + b);
		d2 = 2 * a;
		x = x1;
		y = y1;
		putpixel(x, y, color);
		while (x < x2)
		{
			if (d < 0)
			{
				x++;
				y--;
				d += d1;
			}
			else
			{
				x++;
				d += d2;
			}
			putpixel(x, y, color);
		}
	}
	else
	{
		a = x1 - x2;
		b = y1 - y2;
		d = 2 * a + b;
		d1 = 2 * (a + b);
		d2 = 2 * a;
		x = x1;
		y = y1;
		putpixel(x, y, color);
		while (y > y2)
		{
			if (d < 0)
			{
				x++;
				y--;
				d += d1;
			}
			else
			{
				y--;
				d += d2;
			}
			putpixel(x, y, color);
		}
	}
}

class Shape
{
  public:
	Shape(int n) : stateNum(n) {}
	virtual void Redraw() = 0;
	int stateNum;
	virtual void State(int &state, int x, int y) = 0;
	virtual ~Shape(){};
};

class Line : public Shape
{
  public:
	int x1, y1, x2, y2;
	COLORREF lineColor;
	Line() : Shape(4) {}
	Line(int x1, int y1, int x2, int y2, COLORREF lineColor) : Shape(4), x1(x1), y1(y1), x2(x2), y2(y2), lineColor(lineColor) {}
	virtual void Redraw()
	{
		MPLline(x1, y1, x2, y2, lineColor);
	}
	virtual void State(int &state, int x, int y)
	{
		switch (state)
		{
		case 1:
			x1 = x;
			y1 = y;
			break;
		case 2:
			x2 = x;
			y2 = y;
			lineColor = currentColor;
		case 3:
			MPLline(x1, y1, x2, y2, lineColor);
			break;
		}
	}
	virtual ~Line() {}
};

class SRectangle : public Shape
{
  public:
	int left, right, bottom, top;
	COLORREF lineColor;
	SRectangle() : Shape(4) {}
	SRectangle(int left, int right, int bottom, int top, COLORREF lineColor) : Shape(4), left(left), right(right), bottom(bottom), top(top), lineColor(lineColor) {}
	virtual void Redraw()
	{
		MPLline(left, top, right, top, lineColor);
		MPLline(left, top, left, bottom, lineColor);
		MPLline(left, bottom, right, bottom, lineColor);
		MPLline(right, bottom, right, top, lineColor);
	}
	virtual void State(int &state, int x, int y)
	{
		switch (state)
		{
		case 1:
			left = x;
			top = y;
			break;
		case 2:
			right = x;
			bottom = y;
			lineColor = currentColor;
			Redraw();
			break;
		case 3:
			if (left > right)
			{
				int t = left;
				left = right;
				right = t;
			}
			if (bottom > top)
			{
				int t = bottom;
				bottom = top;
				top = t;
			}
			Redraw();
			break;
		}
	}
	virtual ~SRectangle() {}
};

class SPolygon : public Shape
{
  public:
	std::list<std::pair<int, int>> vertices;
	COLORREF lineColor;
	SPolygon() : Shape(4) {}
	SPolygon(std::list<std::pair<int, int>> vertices, COLORREF lineColor) : Shape(4), vertices(vertices), lineColor(lineColor) {}
	SPolygon(int *points, int length, COLORREF lineColor) : Shape(4), lineColor(lineColor)
	{
		for (int i = 0; i < length / 2; ++i)
		{
			vertices.push_back(std::make_pair(points[2 * i], points[2 * i + 1]));
		}
		vertices.push_back(std::make_pair(points[0], points[1]));
		if(isClockWise())
			vertices.reverse();
	}
	virtual void Redraw()
	{
		std::list<std::pair<int, int>>::iterator it = vertices.begin(), pit;
		for (pit = it, ++it; it != vertices.end(); ++it)
		{
			MPLline(pit->first, pit->second, it->first, it->second, lineColor);
			pit = it;
		}
	}
	virtual void State(int &state, int x, int y)
	{
		switch (state)
		{
		case 1:
			vertices.push_back(std::make_pair(x, y));
			vertices.push_back(std::make_pair(x, y));
			break;
		case 2:
			vertices.back().first = x;
			vertices.back().second = y;
			lineColor = currentColor;
			Redraw();
			break;
		case 3:
			vertices.push_back(std::make_pair(x, y));
			if (abs(x - vertices.front().first) < 5 && abs(y - vertices.front().second) < 5)
			{
				vertices.pop_back();
				vertices.back().first = vertices.front().first;
				vertices.back().second = vertices.front().second;
				if(isClockWise())
					vertices.reverse();
			}
			else
				state -= 2;
			Redraw();
			break;
		}
	}
	//格林公式计算面积正负以判断点的方向（注，以显示而不是坐标的顺时针为准）
	bool isClockWise(){
		int d = 0;
		std::list<std::pair<int, int>>::iterator it, pit;
		it = vertices.begin();
		pit = it;
		++it;
		for(;it != vertices.end();++it)
		{
			d += -(it->second + pit->second)*(it->first - pit->first);
			pit = it;
		}
		return d > 0;
	}
	virtual ~SPolygon() {}
};

//其实这里没有判断溢出的机制
Shape *shapes[10010];
Shape *currentShape;
int shapeIndex = 0;

//使用缓冲技术，减少因图形数量增加时出现的大量Redraw消耗
IMAGE img(640, 480);

void Refresh()
{
	SetWorkingImage();
	putimage(0, 0, &img, SRCCOPY);
}

//有生之年第一次在示例之外用到工厂函数（
class ShapeFactory
{
  public:
	//其实用枚举更好
	static int maxID;
	int shapeID = 1;
	void changeShape()
	{
		if (++shapeID > maxID)
		{
			shapeID = 1;
		}
	}
	Shape *createShape()
	{
		switch (shapeID)
		{
		case 1:
			return new Line();
		case 2:
			return new SRectangle();
		case 3:
			return new SPolygon();
		default:
			return new Line();
		}
	}
};
int ShapeFactory::maxID = 3;

//左裁剪框
SRectangle lclipRect(5, 140, 74, 149, GREEN);
//右裁剪框
SRectangle rclipRect(325, 460, 74, 149, GREEN);

//初始化画布和屏幕(现在已经是用来全部重绘的函数了)
void Init()
{
	//填充背景为白色
	SetWorkingImage(&img);
	setbkcolor(WHITE);
	cleardevice();

	for (int i = 0; i < shapeIndex; ++i)
	{
		if (shapes[i])
			shapes[i]->Redraw();
	}
	SetWorkingImage();
	Refresh();
}

//清理函数（虽然似乎不会执行）
void Clear()
{
	for (int i = 0; i < shapeIndex; ++i)
	{
		delete shapes[i];
	}
}

bool L_B_LineClip(Line &line, const SRectangle &rect);
bool W_A_PolygonClip(SPolygon &polygon, const SRectangle &rect, std::list<std::list<std::pair<int, int>>>& res);

void CopyClipLine(const Line &line)
{
	Line *currentLine = new Line(line.x1 + 320, line.y1, line.x2 + 320, line.y2, line.lineColor);
	if (L_B_LineClip(*currentLine, rclipRect))
	{
		shapes[shapeIndex++] = currentLine;
		SetWorkingImage(&img);
		currentLine->Redraw();
		SetWorkingImage();
		currentLine->Redraw();
	}
	else
	{
		delete currentLine;
		currentLine = NULL;
	}
}

void CopyClipPolygon(const SPolygon &polygon)
{
	std::list<std::pair<int, int>> vlist = polygon.vertices;
	for (std::list<std::pair<int, int>>::iterator it = vlist.begin(); it != vlist.end(); ++it)
	{
		it->first += 320;
	}
	SPolygon *currentPolygon = new SPolygon(vlist, polygon.lineColor);
	std::list<std::list<std::pair<int, int>>> res;
	if (W_A_PolygonClip(*currentPolygon, rclipRect,res))
	{
		for(auto i : res){
			SPolygon *newPolygon = new SPolygon(i,RED);
			shapes[shapeIndex++] = newPolygon;
			SetWorkingImage(&img);
			newPolygon->Redraw();
			SetWorkingImage();
			newPolygon->Redraw();
		}
		
	}
	else
	{
		delete currentPolygon;
		currentPolygon = NULL;
	}
}

bool isDisplay()
{
	HWND hWnd = GetHWnd();
	MessageBox(hWnd, TEXT("左：绘图窗口，右：展示窗口"), TEXT("窗口介绍"), MB_ICONINFORMATION);
	MessageBox(hWnd, TEXT("左键点选多边形的顶点，在起点5像素距离内画点会闭合。右键点选裁剪框的两个顶点。"), TEXT("操作介绍"), MB_ICONINFORMATION);
	return MessageBox(hWnd, TEXT("是否运行展示"), TEXT("功能选择"), MB_YESNO | MB_ICONQUESTION) == IDYES;
}

void Display()
{
	int data[18] = {110, 84, 160, 94, 90, 169, 90, 94, 70, 90, 50, 189, 165, 189, 175, 89, 163, 54};
	SPolygon *sample = new SPolygon(data, 18, RED);
	SetWorkingImage(&img);
	sample->Redraw();
	CopyClipPolygon(*sample);
	shapes[shapeIndex++] = sample;
	Refresh();
}

//主消息循环
void MessageLoop()
{
	MOUSEMSG msg;
	HWND hWnd = GetHWnd();
	char titleBuffer[100];
	WCHAR wTitleBuffer[100];
	size_t len;
	currentShape = new SPolygon;
	int cnt = 0;
	ShapeFactory shapeFactory;
	shapeFactory.shapeID = 3;

	//缺少说明，仅仅用于调试
	while (1)
	{
		msg = GetMouseMsg();
		switch (msg.uMsg)
		{
		case WM_MOUSEMOVE:
			if (cnt)
			{
				Refresh();
				if (currentShape)
					currentShape->State(cnt, msg.x, msg.y);
				else
					lclipRect.State(cnt, msg.x, msg.y);
			}
			break;
		case WM_LBUTTONDOWN:
			Refresh();
			currentShape->State(++cnt, msg.x, msg.y);
			if (++cnt >= currentShape->stateNum)
			{
				shapes[shapeIndex++] = currentShape;
				//这里暂时这样写
				Line *currentLine = dynamic_cast<Line *>(currentShape);
				if (currentLine)
				{
					CopyClipLine(*currentLine);
				}
				else
				{
					SPolygon *currentPolygon = dynamic_cast<SPolygon *>(currentShape);
					if (currentPolygon)
						CopyClipPolygon(*currentPolygon);
				}
				SetWorkingImage(&img);
				currentShape->Redraw();
				SetWorkingImage();
				currentShape = shapeFactory.createShape();
				cnt = 0;
			}
			Refresh();
			//添加使用空间消息
			sprintf_s(titleBuffer, "Space %d/%d used", shapeIndex, 10000);
			len = strlen(titleBuffer);
			MultiByteToWideChar( 0,0, titleBuffer, -1 , wTitleBuffer, 100 ); 
			SetWindowText(hWnd, wTitleBuffer);
			break;
		case WM_RBUTTONDOWN:
			if (currentShape)
			{
				currentColor = GREEN;
				cnt = 0;
				delete currentShape;
				currentShape = NULL;
			}
			Refresh();
			lclipRect.State(++cnt, msg.x, msg.y);
			if (++cnt >= lclipRect.stateNum)
			{
				//重绘并裁剪
				rclipRect = lclipRect;
				rclipRect.left += 320;
				rclipRect.right += 320;
				int currentShapeIndex = shapeIndex;
				for (int i = 0; i < currentShapeIndex; ++i)
				{
					if (shapes[i])
					{
						Line *line = dynamic_cast<Line *>(shapes[i]);
						if (line)
						{
							if (line->x1 > 320 || line->x2 > 320)
							{
								delete shapes[i];
								shapes[i] = 0;
							}
							else
							{
								CopyClipLine(*line);
							}
						}
						else
						{
							SPolygon *polygon = dynamic_cast<SPolygon *>(shapes[i]);
							if (polygon)
							{
								if (!polygon->vertices.empty() && polygon->vertices.front().first > 320)
								{
									delete shapes[i];
									shapes[i] = 0;
								}
								else
								{
									CopyClipPolygon(*polygon);
								}
							}
						}
					}
				}
				Init();
				currentShape = shapeFactory.createShape();
				currentColor = RED;
				cnt = 0;
			}
			Refresh();
			break;
		}
	}
}

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//初始化
	//绘制分割线
	shapes[shapeIndex++] = new Line(320, 0, 320, 480, BLACK);
	//绘制裁剪框
	shapes[shapeIndex++] = &lclipRect;
	Init();
	//展示
	if (isDisplay())
		Display();
	//开始绘制
	MessageLoop();
	//结束绘制
	_getch();	 // 按任意键继续
	closegraph(); // 关闭绘图窗口
	Clear();
	return 0;
}

//以下是主要内容
bool LineSegIntersect(Line &line1,Line &line2,int &x,int &y){
	int delta = (line2.x1-line2.x2)*(line1.y2-line1.y1)-(line2.y1-line2.y2)*(line1.x2-line1.x1);
	if(delta == 0)
		return 0;
	double u = double((line2.x1-line1.x1)*(line2.y2-line2.y1)-(line2.y1-line1.y1)*(line2.x2-line2.x1))/delta;
	if(u < 0 || u > 1)
		return 0;
	double v = double((line2.x1-line1.x1)*(line1.y2-line1.y1)-(line2.y1-line1.y1)*(line1.x2-line1.x1))/delta;
	if(v < 0 || v > 1)
		return 0;
	x = line1.x1 + (line1.x2 - line1.x1) * u;
	y = line1.y1 + (line1.y2 - line1.y1) * u;
	return 1;
}
const int TOP = 0;
const int RIGHT = 1;
const int BOTTOM = 2;
const int LEFT = 3;

struct InterPoint{
	int x, y;
	std::list<InterPoint>::iterator instead;
	int isInterPoint;
	InterPoint(){}
	InterPoint(int x,int y,int isInterPoint):x(x),y(y),isInterPoint(isInterPoint){}
	bool operator!=(const std::pair<int,int>& rhs) const{
		return x != rhs.first || y != rhs.second;
	}
	bool operator<(const InterPoint& rhs) const{
		return x < rhs.x || (x == rhs.x && y < rhs.y);
	}
};

bool W_A_PolygonClip(SPolygon &polygon, const SRectangle& rect,std::list<std::list<std::pair<int, int>>>& res)
{
	std::list<std::pair<int, int>>::iterator it, pit;
	std::list<InterPoint>::iterator tit, ptit;
	std::list<InterPoint> edgePoint[4],liste,listp;
	Line edgeLine[4];

	edgeLine[TOP].x1 = rect.left;
	edgeLine[TOP].x2 = rect.right;
	edgeLine[TOP].y1 = rect.top;
	edgeLine[TOP].y2 = rect.top;
	edgePoint[TOP].push_back(InterPoint(rect.right, rect.top ,0));

	edgeLine[BOTTOM].x1 = rect.left;
	edgeLine[BOTTOM].x2 = rect.right;
	edgeLine[BOTTOM].y1 = rect.bottom;
	edgeLine[BOTTOM].y2 = rect.bottom;
	edgePoint[BOTTOM].push_back(InterPoint(rect.left, rect.bottom,0));

	edgeLine[LEFT].x1 = rect.left;
	edgeLine[LEFT].x2 = rect.left;
	edgeLine[LEFT].y1 = rect.bottom;
	edgeLine[LEFT].y2 = rect.top;
	edgePoint[LEFT].push_back(InterPoint(rect.left, rect.top,0));

	edgeLine[RIGHT].x1 = rect.right;
	edgeLine[RIGHT].x2 = rect.right;
	edgeLine[RIGHT].y1 = rect.bottom;
	edgeLine[RIGHT].y2 = rect.top;
	edgePoint[RIGHT].push_back(InterPoint(rect.right, rect.bottom,0));

	it = polygon.vertices.begin();
	pit = it;
	++it;
	int x,y;
	int cnt = 0;
	for(;it != polygon.vertices.end();++it)
	{
		listp.push_back(InterPoint(pit->first,pit->second,0));
		Line tempLine(pit->first, pit->second, it->first, it->second, RED);
		for(int i = 0; i < 4; ++i){
			if(LineSegIntersect(tempLine,edgeLine[i],x,y))
			{
				++cnt;
				listp.push_back(InterPoint(x, y, cnt));
				edgePoint[i].push_back(InterPoint(x, y, cnt));
			}
		}
		//对每个多边形顶点后的0~2个点进行排序
		tit = listp.end();
		--tit;
		if(tit != listp.begin())
		{
			ptit = tit;
			--ptit;
			if(*tit != *pit && *ptit != *pit){
				if(pow(tit->x - pit->first,2) + pow(tit->y - pit->second,2) < pow(ptit->x - pit->first,2) + pow(ptit->y - pit->second,2))
				{
					std::iter_swap(ptit, tit);
				}
			}
		}
		pit = it;
	}
	listp.push_back(InterPoint(polygon.vertices.front().first,polygon.vertices.front().second,0));
	for (int i = 0; i < 4;++i){
		edgePoint[i].sort();
	}
	edgePoint[BOTTOM].reverse();
	edgePoint[RIGHT].reverse();
	for (int i = 0; i < 4; ++i){
		liste.splice(liste.end(),edgePoint[i]);
	}
	liste.push_front(liste.back());
	//建立双向指针
	tit = liste.begin();
	ptit = listp.begin();
	for (tit = liste.begin(); tit != liste.end();++tit){
		if(tit->isInterPoint){
			for (;;++ptit)
			{
				if(ptit == listp.end())
					ptit = listp.begin();
				if(ptit->isInterPoint == tit->isInterPoint){
					tit->instead = ptit;
					ptit->instead = tit;
					break;
				}
			}
		}
	}
	//判断点在多边形内部的方法包括射线法和角度法等
	while(1){
		std::list<std::pair<int, int>> tempRes;
		for (tit = listp.begin();tit!=listp.end() && !tit->isInterPoint;++tit)
			;
		if(tit==listp.end())
			break;
		ptit = tit;
		--ptit;
		bool isIN = (ptit->x > rect.left && ptit->x < rect.right && ptit->y > rect.bottom && ptit->y < rect.top);
		ptit = tit;
		if (isIN)
			tit = tit->instead;
		tempRes.push_back(std::make_pair(tit->x,tit->y));
		++tit;
		while(&(*ptit)!=&(*tit)){
			tempRes.push_back(std::make_pair(tit->x,tit->y));
			if(tit->isInterPoint){

				tit->isInterPoint = 0;
				tit->instead->isInterPoint = 0;

				tit = tit->instead;
				isIN = !isIN;
				if(&(*ptit)==&(*tit))
					break;
			}
			++tit;
			if(isIN && tit == liste.end())
				tit = liste.begin();
			else if(!isIN && tit == listp.end())
				tit = listp.begin();
		}

		ptit->isInterPoint = 0;

		tempRes.push_back(std::make_pair(tit->x,tit->y));
		res.push_back(tempRes);
	}
	if(res.empty())
	{
		ptit = listp.begin();
		bool isIN = (ptit->x > rect.left && ptit->x < rect.right && ptit->y > rect.bottom && ptit->y < rect.top);
		if(isIN)
		{
			res.push_back(polygon.vertices);
			return 1;
		}
		return 0;
	}
		
	return 1;
}

bool L_B_LineClip(Line &line, const SRectangle &rect)
{
	int p1 = line.x1 - line.x2;
	int q1 = line.x1 - rect.left;
	int q2 = rect.right - line.x1;
	int p3 = line.y1 - line.y2;
	int q3 = line.y1 - rect.bottom;
	int q4 = rect.top - line.y1;
	double umax = 0, umin = 1;
	if (!p1)
	{
		if (q1 < 0 || q2 < 0)
			return 0;
		if (p3 < 0)
		{
			double u3 = double(q3) / p3, u4 = double(q4) / -p3;
			if (u3 > umax)
				umax = u3;
			if (u4 < umin)
				umin = u4;
		}
		else
		{
			double u3 = double(q3) / p3, u4 = double(q4) / -p3;
			if (u4 > umax)
				umax = u4;
			if (u3 < umin)
				umin = u3;
		}
	}
	else if (!p3)
	{
		if (q3 < 0 || q4 < 0)
			return 0;
		if (p1 < 0)
		{
			double u1 = double(q1) / p1, u2 = double(q2) / -p1;
			if (u1 > umax)
				umax = u1;
			if (u2 < umin)
				umin = u2;
		}
		else
		{
			double u1 = double(q1) / p1, u2 = double(q2) / -p1;
			if (u2 > umax)
				umax = u2;
			if (u1 < umin)
				umin = u1;
		}
	}
	else
	{
		if (p3 < 0)
		{
			double u3 = double(q3) / p3, u4 = double(q4) / -p3;
			if (u3 > umax)
				umax = u3;
			if (u4 < umin)
				umin = u4;
		}
		else
		{
			double u3 = double(q3) / p3, u4 = double(q4) / -p3;
			if (u4 > umax)
				umax = u4;
			if (u3 < umin)
				umin = u3;
		}
		if (p1 < 0)
		{
			double u1 = double(q1) / p1, u2 = double(q2) / -p1;
			if (u1 > umax)
				umax = u1;
			if (u2 < umin)
				umin = u2;
		}
		else
		{
			double u1 = double(q1) / p1, u2 = double(q2) / -p1;
			if (u2 > umax)
				umax = u2;
			if (u1 < umin)
				umin = u1;
		}
	}
	if (umax > umin)
		return 0;
	int x1 = line.x1 + umax * (line.x2 - line.x1);
	int y1 = line.y1 + umax * (line.y2 - line.y1);
	line.x2 = line.x1 + umin * (line.x2 - line.x1);
	line.y2 = line.y1 + umin * (line.y2 - line.y1);
	line.x1 = x1;
	line.y1 = y1;
	return 1;
}