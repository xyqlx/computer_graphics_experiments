//////////////////////////////////////////////////////
// 程序名称：用中点画线扫描转换算法，能绘制任意斜率的直线
// 功 能：通过调用此函数绘制图案，图案中包含各种斜率
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-3-7
#include <conio.h>
#include <graphics.h>
#include <cmath>

void MPLline(int x1,int y1,int x2,int y2,COLORREF color);

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//填充背景为白色
	setbkcolor(WHITE);
	cleardevice();
	//开始绘制
	setlinecolor(RED);
	
	//画从中心向四周辐射的多条线段
	const int n = 72;
	float PI = float(acos(0) * 2);
	for (int i = 0; i < n; i++) {
		int px = 320 + int(200 * cos(i * 2 * PI / n));
		int py = 240 + int(200 * sin(i * 2 * PI / n));
		MPLline(320, 240, px, py, RED);
	}

	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
	return 0;
}

void MPLline(int x1,int y1,int x2,int y2,COLORREF color){
	int a,b,d1,d2,d,x,y;
	if(x1>x2){
		x1 ^= x2;
		x2 ^= x1;
		x1 ^= x2;
		y1 ^= y2;
		y2 ^= y1;
		y1 ^= y2;
	}
	x = x2 - x1;
	y = y2 - y1;
	if(y >= x){
		a=x1-x2;b=y2-y1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(y<y2)
		{
			if(d<0) {x++;y++; d+=d1;}
			else {y++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else if(y <= x && y >= 0){
		a=y1-y2;b=x2-x1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(x<x2)
		{
			if(d<0) {x++;y++; d+=d1;}
			else {x++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else if(y>=-x){
		a=y2-y1;b=x2-x1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(x<x2)
		{
			if(d<0) {x++;y--; d+=d1;}
			else {x++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else{
		a=x1-x2;b=y1-y2;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(y>y2)
		{
			if(d<0) {x++;y--; d+=d1;}
			else {y--; d+=d2;}
			putpixel(x,y,color);
		}
	}
}