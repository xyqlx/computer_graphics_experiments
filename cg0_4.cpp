﻿//////////////////////////////////////////////////////
// 程序名称：绘制并填充三角形
// 功 能：绘制并填充三角形
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-2-28
#include <conio.h>
#include <graphics.h>
#include <cmath>

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//填充背景为白色
	setbkcolor(WHITE);
	cleardevice();
	//开始绘制
	setlinecolor(RED);
	int point1[] = { 100,0,128,0,48,80,150,80,170,100,0,100,100,0 };
	int point2[] = { 0,100,20,120,220,120,128,28,113,43,170,100,0,100 };
	int point3[] = { 76,80,128,28,220,120,228,100,128,0,48,80,76,80 };
	drawpoly(7, point1);
	drawpoly(7, point2);
	drawpoly(7, point3);
	setfillcolor(RED);
	floodfill(101, 1, WHITE, FLOODFILLSURFACE);
	setfillcolor(YELLOW);
	floodfill(20, 110, WHITE, FLOODFILLSURFACE);
	setfillcolor(BLUE);
	floodfill(128, 1, WHITE, FLOODFILLSURFACE);
	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
}