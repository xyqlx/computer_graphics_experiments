﻿//////////////////////////////////////////////////////
// 程序名称：绘制金刚石图案
// 功 能：绘制n=7的金刚石图案
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-2-28
#include <conio.h>
#include <graphics.h>
#include <cmath>

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//填充背景为白色
	setbkcolor(WHITE);
	cleardevice();
	//开始绘制
	//设置线的颜色
	COLORREF linecolor = RGB(255, 0, 0);
	setlinecolor(linecolor);
	//n等分
	const int n = 7;
	//PI
	float PI = float(acos(0) * 2);
	//计算
	int px[n], py[n];
	for (int i = 0; i < n; i++) {
		px[i] = 320 + int(100 * cos(i * 2 * PI / n));
		py[i] = 240 + int(100 * sin(i * 2 * PI / n));
	}
	//绘图
	for (int i = 0; i < n; ++i)
		for (int j = 0; j < i; ++j) {
			line(px[i], py[i], px[j], py[j]);
		}
	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
}