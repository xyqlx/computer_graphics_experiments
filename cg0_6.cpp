//////////////////////////////////////////////////////
// 程序名称：自动生成，待补充
// 功 能：自动生成，待补充
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-3-6
#include <conio.h>
#include <graphics.h>
#include <cmath>


void MPLline(int x1,int y1,int x2,int y2,COLORREF color);

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//填充背景为白色
	setbkcolor(WHITE);
	cleardevice();
	//开始绘制
	setlinecolor(RED);
	
	int xl[] = {-80,80,-80,80};
	int yl[] = {-50,-50,50,50};

	for(int i=0;i<4;++i){
		MPLline(200,200,200+xl[i],200+yl[i],RED);
	}


	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
	return 0;
}

void MPLline(int x1,int y1,int x2,int y2,COLORREF color){
	int a,b,d1,d2,d,x,y;
	if(x1>x2){
		x1 ^= x2;
		x2 ^= x1;
		x1 ^= x2;
		y1 ^= y2;
		y2 ^= y1;
		y1 ^= y2;
	}
	x = x2 - x1;
	y = y2 - y1;
	if(y > x){
		a=x1-x2;b=y2-y1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(y<y2)
		{
			if(d<0) {x++;y++; d+=d1;}
			else {y++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else if(y < x && y > 0){
		a=y1-y2;b=x2-x1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(x<x2)
		{
			if(d<0) {x++;y++; d+=d1;}
			else {x++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else if(y>-x){
		a=y2-y1;b=x2-x1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(x<x2)
		{
			if(d<0) {x++;y--; d+=d1;}
			else {x++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else{
		a=x1-x2;b=y1-y2;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(y>y2)
		{
			if(d<0) {x++;y--; d+=d1;}
			else {y--; d+=d2;}
			putpixel(x,y,color);
		}
	}
}