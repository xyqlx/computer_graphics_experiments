//////////////////////////////////////////////////////
// 程序名称：设计具有宽度的圆算法
// 功 能：画一些圆
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-3-7
#include <conio.h>
#include <graphics.h>
#include <cmath>

void BoldCircle(int x0, int y0, int r, int width, int color);
int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//填充背景为白色
	setbkcolor(WHITE);
	cleardevice();
	//开始绘制
	setlinecolor(RED);
	
	//画圆
	for (int i = 0; i < 10;++i)
		BoldCircle(320, 240, 1 + i*20,i+1, RED);

	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
	return 0;
}

inline void PutCirclePixel(int x0,int y0,int x,int y,COLORREF color){
	putpixel(x + x0, y + y0, color);
	putpixel(x + x0, -y + y0, color);
	putpixel(y + x0, x + y0, color);
	putpixel(y + x0, -x + y0, color);
	putpixel(-x + x0, y + y0, color);
	putpixel(-x + x0, -y + y0, color);
	putpixel(-y + x0, x + y0, color);
	putpixel(-y + x0, -x + y0, color);
}

void BoldCircle(int x0,int y0,int r,int width,int color)
{
	int x, y, d;
	x = 0;
	y = r;
	d = 1 - r;
	width >>= 1;
	PutCirclePixel(x0, y0, x, y, color);
	for (int i = 1; i <= width;++i)
	{
		PutCirclePixel(x0, y0, x, y+i, color);
		PutCirclePixel(x0, y0, x, y-i, color);
	}
	while (x < y)
	{
		if (d < 0)
		{
			d += 2 * x + 3;
			x++;
		}
		else
		{
			d += 2 * (x - y) + 5;
			x++;
			y--;
		}
		PutCirclePixel(x0, y0, x, y, color);
		for (int i = 1; i <= width;++i)
		{
			PutCirclePixel(x0, y0, x, y+i, color);
			PutCirclePixel(x0, y0, x, y-i, color);
		}
	}
	//补画一个小三角形
	r = (r >> 1) * sqrt(2.0f) + 1;
	for (y = r; y <= r + width; ++y)
	{
		int ry;
		for (x = r,ry=y; x <= ry;++x,--ry){
			PutCirclePixel(x0, y0, x, ry, color);
		}
	}
}