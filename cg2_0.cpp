//////////////////////////////////////////////////////
// 程序名称：线段裁剪平台
// 功 能：自动生成，待补充
// 编译环境：Visual Studio 2017，EasyX_20190219(beta)
// 作 者：xyqlx<mxxyqlx@qq.com>
// 最后修改：2019-3-21
#include <conio.h>
#include <graphics.h>
#include <cmath>

COLORREF currentColor = RED;

void MPLline(int x1,int y1,int x2,int y2,COLORREF color){
	int a,b,d1,d2,d,x,y;
	if(x1>x2){
		x1 ^= x2;
		x2 ^= x1;
		x1 ^= x2;
		y1 ^= y2;
		y2 ^= y1;
		y1 ^= y2;
	}
	x = x2 - x1;
	y = y2 - y1;
	if(y >= x){
		a=x1-x2;b=y2-y1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(y<y2)
		{
			if(d<0) {x++;y++; d+=d1;}
			else {y++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else if(y <= x && y >= 0){
		a=y1-y2;b=x2-x1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(x<x2)
		{
			if(d<0) {x++;y++; d+=d1;}
			else {x++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else if(y>=-x){
		a=y2-y1;b=x2-x1;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(x<x2)
		{
			if(d<0) {x++;y--; d+=d1;}
			else {x++; d+=d2;}
			putpixel(x,y,color);
		}
	}
	else{
		a=x1-x2;b=y1-y2;
		d=2*a+b;
		d1=2*(a+b);
		d2=2*a;
		x=x1;
		y=y1;
		putpixel(x,y,color);
		while(y>y2)
		{
			if(d<0) {x++;y--; d+=d1;}
			else {y--; d+=d2;}
			putpixel(x,y,color);
		}
	}
}

class Shape{
public:
	Shape(int n):stateNum(n){}
	virtual void Redraw()=0;
	int stateNum;
	virtual void State(int state,int x,int y)=0;
	virtual ~Shape(){};
};

class Line:public Shape{
public:
  int x1, y1, x2, y2;
  COLORREF lineColor;
  Line():Shape(4){}
  Line(int x1,int y1,int x2,int y2,COLORREF lineColor):Shape(4),x1(x1),y1(y1),x2(x2),y2(y2),lineColor(lineColor){}
  virtual void Redraw()
  {
	  MPLline(x1, y1, x2, y2, lineColor);
  }
  virtual void State(int state,int x,int y){
	  switch(state){
		  case 1:
				x1 = x;
				y1 = y;
				break;
		  case 2:
			  	x2 = x;
			  	y2 = y;
				lineColor = currentColor;
		  case 3:
			  MPLline(x1, y1, x2, y2, lineColor);
			  break;
	  }
  }
  virtual ~Line(){}
};

//其实这里没有判断溢出的机制
Shape *shapes[10010];
Shape *currentShape;
int shapeIndex = 0;

//使用缓冲技术，减少因图形数量增加时出现的大量Redraw消耗
IMAGE img(640, 480);

void Refresh(){
	SetWorkingImage();
	putimage(0, 0, &img, SRCCOPY);
}

//有生之年第一次在示例之外用到工厂函数（
class ShapeFactory{
public:
//其实用枚举更好
  static int maxID;
  int shapeID = 1;
  void changeShape(){
	  if(++shapeID > maxID){
		  shapeID = 1;
	  }
  }
  Shape *createShape()
  {
	  switch (shapeID)
	  {
	  case 1:
		  return new Line();
	  default:
		  return new Line();
	  }
	}
};
int ShapeFactory::maxID = 1;

//初始化画布和屏幕
void Init(){
	//填充背景为白色
	SetWorkingImage(&img);
	setbkcolor(WHITE);
	cleardevice();
	//绘制分割线
	shapes[shapeIndex++] = new Line(320, 0, 320, 480, BLACK);
	//绘制裁剪框
	shapes[shapeIndex++] = new Line(100, 100, 200, 100, GREEN);
	shapes[shapeIndex++] = new Line(100, 200, 100, 100, GREEN);
	shapes[shapeIndex++] = new Line(200, 100, 200, 200, GREEN);
	shapes[shapeIndex++] = new Line(200, 200, 100, 200, GREEN);
	for (int i = 0; i < shapeIndex;++i){
		shapes[i]->Redraw();
	}
	//画文字提示
	settextcolor(BLACK);
	moveto(5, 5);
	outtext(TEXT("left click to select startpoint"));
	moveto(5, 20);
	outtext(TEXT(" and endpoint in the left window"));
	SetWorkingImage();
	Refresh();
}

//清理函数（虽然似乎不会执行）
void Clear(){
	for (int i = 0; i < shapeIndex;++i){
		delete shapes[i];
	}
}

//主消息循环
void MessageLoop(){
	MOUSEMSG msg;
	currentShape = new Line;
	int cnt = 0;
	ShapeFactory shapeFactory;

	//缺少说明，仅仅用于调试
	while(1){
		msg = GetMouseMsg();
		switch(msg.uMsg){
			case WM_MOUSEMOVE:
				if(cnt){
					Refresh();
					currentShape->State(cnt, msg.x, msg.y);
				}
				break;
			case WM_LBUTTONDOWN:
				Refresh();
				currentShape->State(++cnt, msg.x, msg.y);
				if(++cnt >= currentShape->stateNum){
					shapes[shapeIndex++] = currentShape;
					SetWorkingImage(&img);
					currentShape->Redraw();
					SetWorkingImage();
					currentShape = shapeFactory.createShape();
					cnt = 0;
				}
				Refresh();
				break;
			case WM_RBUTTONDOWN:
				shapeFactory.changeShape();
				if(currentShape)
					delete currentShape;
				currentShape = shapeFactory.createShape();
				cnt = 0;
		}
	}
}

int main()
{
	// 创建绘图窗口，大小为 640x480 像素
	initgraph(640, 480);
	//初始化
	Init();
	//开始绘制
	MessageLoop();
	//结束绘制
	_getch();              // 按任意键继续
	closegraph();          // 关闭绘图窗口
	Clear();
	return 0;
}